/* Code for a implement the state feedback control for a RLC circuit
	using a DAC module TLC5615.

	Code by: Álvaro Javier Vargas Miranda ajvargasm@unal.edu.co
	AvalonXCV
*/
#include <Arduino.h>
#include <SPI.h>
#include <HardwareSerial.h>

#define sl Serial

/*
  | Arduino	| DAC	|
  |-----------|-------|
  | 13		| SCLK 	|
  | 12		| DOUT 	|
  | 11		| DIN 	|
  | 08		| CS 	|

*/

// Funciones
void mesureStates(void);
void calculateControl(double ref);
void actuactorOutput(double volt);
void displayData(void);

// Pines
const int csDAC = 8;
const int nodeA = A0;
const int nodeB = A1;
const int DAC_REF = A2;

// Variables
bool fStart = true;
double vRef = 0.0;
double uC = 0.0;
double vC = 0.0;
double iL = 0.0;
float vMAX = 0.0;
float error = 0.0;

void setup()
{
  //Setup Serial
  sl.begin(115200);
  //Setup SPI
  SPI.begin();
  //Setup PIN
  pinMode(csDAC, OUTPUT);
  digitalWrite(csDAC, 0x01);
  //Setup ADC
  float vRead = 0;
  for (byte i = 1; i <= 4; i++)
  {
    vRead += analogRead(DAC_REF) * (5.0 / 1023.0) * 2.0;
    delay(250);
  }
  vMAX = vRead / 4.0;

  //Start
  sl.print("\n#Program Start!\n");
  vRef = 0;
  sl.println("#Test de voltajes");
  while (vRef <= 5) {
    actuactorOutput(vRef);
    delay(500);
    mesureStates();
    displayData();
    vRef++;
  }
  sl.println("#Test terminado.");
  sl.println("Ref; Vc; Il; uC");
  actuactorOutput(0.0);
  delay(1000);
}

void loop()
{
  if (fStart)
  {
    fStart = false;
    int counter = 0;
    unsigned long In_timestamp = millis();
    for (int c = 30; c >= 1; c--)
    {
      unsigned long sampleStamp = 0;
      unsigned long timeStamp = millis();
      do
      { // High State
        sampleStamp = micros();
        counter ++;

        vRef = 2.0; //[v]

        double vA = analogRead(nodeA) * (5.0 / 1023.0);
        double vB = analogRead(nodeB) * (5.0 / 1023.0);

        vC = vA;
        iL = (vB - vA) / 220;

        error = error + (vRef - vC);
        uC = -1 * (1.9243 * vC + 0.0158 * iL - 0.2930 * error);
        if (uC > vMAX) {
          uC = vMAX;
        }
        if (uC < 0.0) {
          uC = 0.0;
        }

        unsigned int bitValue;
        bitValue = (uC * (1023.0 / vMAX));
        bitValue = (bitValue & 0x3ff) << 2;

        // Selecciona el DAC
        digitalWrite(csDAC, 0x00);

        // Activa la transmision
        SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE0));
        SPI.transfer16(bitValue);
        SPI.endTransaction();

        // Libera el DAC
        digitalWrite(csDAC, 0x01);

        sl.print(vRef, 2);
        sl.print("; ");
        sl.print(vC, 4);
        sl.print("; ");
        sl.print(iL * 1000, 5);
        sl.print("; ");
        sl.print(uC, 4);
        sl.println(";");

        sampleStamp = micros() - sampleStamp;
        delayMicroseconds(7e03 - sampleStamp); //Garantizar un Ts fijo, cercano a los 7ms
      } while (millis() - timeStamp < 1e03);

      timeStamp = millis();
      do
      { // Low State
        sampleStamp = micros();
        counter ++;

        vRef = 2.0; //[v]

        double vA = analogRead(nodeA) * (5.0 / 1023.0);
        double vB = analogRead(nodeB) * (5.0 / 1023.0);

        vC = vA;
        iL = (vB - vA) / 220;

        error = error + (vRef - vC);
        uC = -1 * (1.9243 * vC + 0.0158 * iL - 0.2930 * error);
        if (uC > vMAX) {
          uC = vMAX;
        }
        if (uC < 0.0) {
          uC = 0.0;
        }

        unsigned int bitValue;
        bitValue = (uC * (1023.0 / vMAX));
        bitValue = (bitValue & 0x3ff) << 2;

        // Selecciona el DAC
        digitalWrite(csDAC, 0x00);

        // Activa la transmision
        SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE0));
        SPI.transfer16(bitValue);
        SPI.endTransaction();

        // Libera el DAC
        digitalWrite(csDAC, 0x01);

        sl.print(vRef, 2);
        sl.print("; ");
        sl.print(vC, 4);
        sl.print("; ");
        sl.print(iL * 1000, 5);
        sl.print("; ");
        sl.print(uC, 4);
        sl.println(";");

        sampleStamp = micros() - sampleStamp;
        delayMicroseconds(7e03 - sampleStamp); //Garantizar un Ts fijo, cercano a los 7ms
      } while (millis() - timeStamp < 1e03);
    }

    float duracion = millis() - In_timestamp;
    sl.print("Tiempo[ms]: "); sl.println(duracion);
    sl.print("Muestras:"); sl.println(counter);
    sl.print("Muestreo:"); sl.println(duracion / counter);
    sl.print("vMAX: "); sl.println(vMAX);
    actuactorOutput(0.0);

  }
}

/*!
	@function mesureStates
	@abstract Mesure the voltages on Node A and Nobe B, and calc Vc, Il
*/
void mesureStates(void)
{
  double vA = analogRead(nodeA) * (5.0 / 1023.0);
  double vB = analogRead(nodeB) * (5.0 / 1023.0);
  vC = vA;
  iL = (vB - vA) / 220;
}

/*!
	@function calculateControl
	@abstract Calculate the control action based on the state feedback controller
	@param ref: the input reference for the controller
*/
void  calculateControl(double ref)
{
  uC = (1.3403 * ref) - (0.1600 * vC + 0.0015 * iL);
  if (uC > vMAX) {
    uC = vMAX;
  }
  if (uC < 0.0) {
    uC = 0.0;
  }
}

/*!
	@function actuatorOutput
    @abstract Send the volt value to DAC device

	@param volt value for the DAC output.
	[ min:0.000v | res: 0.005v | max:vMAX v ]
*/
void actuactorOutput(double volt)
{
  unsigned int bitValue;
  bitValue = (volt * (1023.0 / vMAX));
  bitValue = (bitValue & 0x3ff) << 2;

  // Selecciona el DAC
  digitalWrite(csDAC, 0x00);

  // Activa la transmision
  SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE0));
  SPI.transfer16(bitValue);
  SPI.endTransaction();

  // Libera el DAC
  digitalWrite(csDAC, 0x01);

}

void displayData(void)
{
  sl.print(vRef, 2);
  sl.print("; ");
  sl.print(vC, 5);
  sl.print("; ");
  sl.print(iL, 5);
  sl.print("; ");
  sl.print(uC, 4);
  sl.println(";");
}
