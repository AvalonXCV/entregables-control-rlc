
clc
%C=0.047e-6;L=15e-3;R2=1000;R3=R2; R1=220;
C=470e-6;L=1e-3;R2=1000;R3=R2; R1=220;
Am=[-2/(R2*C) 1/C;-1/L -R1/L];
Bm=[1/(C*R2);1/L];
um=1;


%discretización
Ts=0.007;
sys=ss(Am,Bm,[1 0],0); %se toma como salida el primer estado
sysd=c2d(sys,Ts,'zoh')


%feedback

Kd = dlqr(sysd.A,sysd.B,1*eye(2),0.1,0)
eig(sysd.A-sysd.B*Kd)
p=1/(sysd.C*inv(eye(2)-(sysd.A-sysd.B*Kd))*sysd.B); %ganancia prealimentada

%Dise�o acci�n integral
Kde = dlqr([sysd.A zeros(2,1);-sysd.C 1],[sysd.B;0],eye(3),0.1,0);
eig([sysd.A zeros(2,1);-sysd.C 1]-[sysd.B;0]*Kde)

%referencia
Tt=2;
Nt=round(Tt/Ts);
tr = linspace(0,2,Nt);
ref = 0.5*square(2*pi*tr)+2.0;

%lazo de control
x(:,1)=[0 0]';
x1(:,1)=[0 0]';
xe(1)=0; %condici�n inicial estado integral
u(1)=0;
ui(1)=0;

for i=1:Nt
    u(i+1)=-Kd*x(:,i)+p*ref(i);
    x(:,i+1)=sysd.A*x(:,i)+sysd.B*u(i+1);
    
    xe(i+1)=xe(i)+(ref(i)-sysd.C*x1(:,i)); %estado integral, hay que irlo calculando
    ui(i+1)=-Kde(1:2)*x1(:,i)-Kde(3)*xe(i); %acci�n de control con estado integral
    x1(:,i+1)=sysd.A*x1(:,i)+sysd.B*ui(i+1);
end
figure (1)
cla
td=0:Ts:Ts*(Nt-1);
plot(tr,ref,'.-',td,x(1,1:end-1),td,x1(1,1:end-1),'b')
legend('Referencia','Ganancia Prealimentada', 'Acci�n Integral')
grid on

figure (2)
cla
plot(td,u(1:end-1))
grid on

%% Sistema con perturbación de la bobina

C=470e-6;L=1e-3;R2=1000;R3=R2; R1=220; RD = 200;
Ad=[(-1/C)*(1/R2 + 1/R3 + 1/RD), 1/C;-1/L, -R1/L];
Bd=[(1/C)*(1/R2 + 1/RD);1/L];

%discretización
Ts=0.007;
sys2=ss(Ad,Bd,[1 0],0); %se toma como salida el primer estado
sys2d=c2d(sys2,Ts,'zoh')

%referencia
Tt=2;
Nt=round(Tt/Ts);
tr = linspace(0,2,Nt);
ref = 0.5*square(2*pi*tr)+2.0;

%lazo de control
x(:,1)=[0 0]';
x1(:,1)=[0 0]';
xe(1)=0; %condici�n inicial estado integral
u(1)=0;
ui(1)=0;

for i=1:Nt
    u(i+1)=-Kd*x(:,i)+p*ref(i);
    x(:,i+1)=sys2d.A*x(:,i)+sys2d.B*u(i+1);
    
    xe(i+1)=xe(i)+(ref(i)-sys2d.C*x1(:,i)); %estado integral, hay que irlo calculando
    ui(i+1)=-Kde(1:2)*x1(:,i)-Kde(3)*xe(i); %acci�n de control con estado integral
    x1(:,i+1)=sys2d.A*x1(:,i)+sys2d.B*ui(i+1);
end
figure (3)
cla
td=0:Ts:Ts*(Nt-1);
plot(tr,ref,'.-',td,x(1,1:end-1),td,x1(1,1:end-1),'b')
legend('Referencia','Ganancia Prealimentada', 'Acción Integral')
grid on

figure (4)
cla
plot(td,u(1:end-1))
grid on
    
 %%   
% [T,x]=ode45(@(t,x) Am*x+Bm*um, [0 1], [0 0]');
% 
% plot(T,x(:,1))
% figure
% plot(T,x(:,2))
% 
% Lambda = poly(Am);
% 
% zita=0.5*Lambda(2)/sqrt(Lambda(1)*Lambda(3))
% wn=sqrt(Lambda(3)/Lambda(1))
% tsd=4/(zita*wn)
% tsd1=4*Lambda(1)/Lambda(2)

