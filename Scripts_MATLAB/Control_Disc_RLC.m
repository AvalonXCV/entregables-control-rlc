    close all
clc

%C=0.047e-6;L=15e-3;R2=1000;R3=R2; R1=220;
C=470e-6;L=1e-3;R2=1000;R3=R2; R1=220;
Am=[-2/(R2*C) 1/C;-1/L -R1/L];
Bm=[1/(C*R2);1/L];
um=1;

p=eig(Am)

%discretizaci�n
Ts=7.0e-03;
sys=ss(Am,Bm,[1 0;0 1],0);                                       
sysd=c2d(sys,Ts,'zoh')
%feedback
%eig(sysd.A-sysd.B*[-1 -219.9630])
Kd = dlqr(sysd.A,sysd.B,[0.75 0;0 0.75],1.75,0)
eig(sysd.A-sysd.B*Kd)                                

r=4;
Tt=1;
Nt=round(Tt/Ts)
x(:,1)=[0 0]';
u(1)=0;
for i=1:Nt
    u(i+1)=-Kd*x(:,i) + 1.0*r;
    x(:,i+1)=sysd.A*x(:,i)+sysd.B*u(i+1);
end
figure (1)
hold on
td=0:Nt-1;
%plot(td,x(1,1:end-1))
scatter(td,x(1,1:end-1),'.')
grid on

figure (2)
plot(td,u(1:end-1))
grid on
    
 %%   
% [T,x]=ode45(@(t,x) Am*x+Bm*um, [0 1], [0 0]');
% 
% plot(T,x(:,1))
% figure
% plot(T,x(:,2))
% 
% Lambda = poly(Am);
% 
% zita=0.5*Lambda(2)/sqrt(Lambda(1)*Lambda(3))
% wn=sqrt(Lambda(3)/Lambda(1))
% tsd=4/(zita*wn)
% tsd1=4*Lambda(1)/Lambda(2)

